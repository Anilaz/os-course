// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// Introduction to programming in C

#include<stdio.h>
int main() 
{ 
  //char a = 'p'; // ASCI 112 -- Binäre Representation von 112
  //char b = ''\0';
  //char c = '(';
  
  // Boolean False:
  // char d = '\0';
  // char f = 0; // ASCII 

  // Boolean True:
  // char t = 1; 
  
  
  printf("Hello World\n"); return 0;
}