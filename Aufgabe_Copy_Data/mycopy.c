// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// This program copies a file using the following command in terminal:
// mycopy [-b n] src dest, 
// The flag b is optional, and, if chosen, represents
// the size of buffer in bytes that is to be used for copying

#define _POSIX_C_SOURCE 200112L

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

// size_t is an unsigned integer type of at least 16 bit
void getparams(int argc, char **argv, size_t *buflen, char **inname,
               char **outname);

void getfilehandles(char *inname, char *outname, int *infd, int *outfd);

void copy(int infd, int outfd, size_t buflen);

int main(int argc, char **argv)
{

  char *inname;
  char *outname;
  size_t buflen;
  int infd;
  int outfd;

  getparams(argc, argv, &buflen, &inname, &outname);
  getfilehandles(inname, outname, &infd, &outfd);

  copy(infd, outfd, buflen);

  if (close(outfd) == -1) {
    perror("Fehler beim Schließen der Ausgabedatei");
    exit(EXIT_FAILURE);
  }

  if (close(infd) == -1) {
    perror("Fehler beim Schließen der Eingabedatei");
    exit(EXIT_FAILURE);
  }

  return EXIT_SUCCESS;
}

void getparams(int argc, char **argv, size_t *buflen, char **inname,
               char **outname)
{
  int opterrflag = 0;
  int opt;
  intmax_t buflenreq = 1; // Maximum width integer type 

  // Standard-Wert
  *buflen = 1;

  /* Kommandozeilenoptionen verarbeiten */
  // The getopt() function is a command-line parser ...
  // The parameters argc and argv are the argument count and argument array as passed to main() (see exec() ). 
  // The argument optstring is a string of recognized option characters; 
  // if a character is followed by a colon, the option takes an argument.
  while ((opt = getopt(argc, argv, "b:")) != -1) {
    switch (opt) {
    case 'b':
        // sscanf() used to read formatted input from a string
      opterrflag = sscanf(optarg, "%jd", &buflenreq) != 1 || (buflenreq <= 0);
      break;
    case '?':
      opterrflag = 1;
      break;
    }
  }

  if (optind < argc) {
    *inname = argv[optind++];
  } else {
    opterrflag = 1;
  }

  if (optind < argc) {
    *outname = argv[optind];
  } else {
    opterrflag = 1;
  }

  if (opterrflag) {
    fprintf(stderr,
            "Benutzung:\n"
            "\n"
            "  %s [-b n] <Eingabedatei> <Ausgabedatei>\n"
            "\n" "b[=1]: Puffergröße n > 0\n"
            "       Das obere Limit von n ist systemabhängig, mindestens %u.\n"
            "       Zu große Werte werden auf das Maximum gekappt.\n",
            argv[0], _POSIX_SSIZE_MAX);
    exit(EXIT_FAILURE);
  }

  *buflen = (buflenreq > SSIZE_MAX)? SSIZE_MAX : buflenreq;
}

void getfilehandles(char *inname, char *outname, int *infd, int *outfd)
{
  if ((*infd = open(inname, O_RDONLY)) == -1) {  //http://codewiki.wikidot.com/c:system-calls:open
    perror("Fehler beim Öffnen der Eingabedatei");
    exit(EXIT_FAILURE);
  }
  
  struct stat buf;
  if((stat(outname, &buf) == -1) && (errno == ENOENT)) {
    if ((*outfd = creat(outname,0666)) == -1) {       // http://man7.org/linux/man-pages/man2/open.2.html
      perror("Fehler beim Erstellen der Zieldatei");
      exit(EXIT_FAILURE);
    }
  }
  else {
    char usrinput;
    printf("Die Zieldatei ist schon vorhanden. Soll sie überschrieben werden? y/n: ");
    scanf("%c", &usrinput);
    switch (usrinput) {
      case 'y': case 'Y':
        if ((*outfd = creat(outname,0666)) == -1) { 
          perror("Fehler beim Erstellen der Zieldatei");
          exit(EXIT_FAILURE);
        }
        break;
      case 'n': case 'N':
        printf("Alles klar. Die Datei wurde nicht überschrieben.\n");
        exit(EXIT_FAILURE);
      default:
        printf("Falsche Eingabe!\n");
        exit(EXIT_FAILURE);
    }
  } 
}

void copy(int infd, int outfd, size_t buflen)
{
  char buffer[buflen];
  int count;
  while ((count = read(infd, buffer, buflen)) > 0) {  //http://man7.org/linux/man-pages/man2/read.2.html
    write(outfd, buffer, count);                      //http://man7.org/linux/man-pages/man2/write.2.html
  } // Falls read weniger als buflen Zeichen (z.B. am Ende der Datei) eingelesen hat, 
    // dann steht dieser Wert in count, und genau count Zeichen sollen in outfd geschrieben werden.
}
