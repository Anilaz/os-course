#!/bin/bash

# Solutions to the Operating Systems course at the University of Tuebingen
# Author: Zalina Baysarova

# This is a bash script for executing mycopy program
# with various buffer sizes

for ((I=0; $I<17; I++))
do
  echo "Buffer 2^$I = $[2**$I]"
  time ./mycopystable -b $[2**$I] /tmp/write.man /tmp/copy$I
  echo
  echo
done

# Zum Ausführen: sh testbuffer.sh &> testbuffer_output.txt
# http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-3.html

