// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

#define _POSIX_C_SOURCE 200112L
#include <stdio.h>
#include <unistd.h> // access to the POSIX operating system API
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>



#define TSIZE 100 // Number of symbols to be read
char text[TSIZE + 2]; // Additional element for \n -- the user presses enter
sem_t* volatile sem; // The variable sem is volatile. Whereas volatile sem_t *sem means: the target of the pointer is volatile

void read_input() 
{
  printf("Enter text of maximal size 100 symbols:\n");
  fgets(text, TSIZE + 2, stdin); // http://man7.org/linux/man-pages/man3/stdin.3.html
  text[strlen(text) - 1] = '\0'; // Remove \n from the string
}

void send_bits()
{
  char symbol;
  pid_t ppid = getppid();
  for (int i = 0; i < (strlen(text) + 1); i++) { // End of text -- \0 --  is sent as well
    for (int j = 7; j >= 0; j--) {
      symbol = text[i];
      symbol = symbol & (1 << j);
      sem_wait(sem);
      if (!symbol) { // Bit value: 0
        kill(ppid, SIGUSR1); // http://man7.org/linux/man-pages/man2/kill.2.html
      }
      else { // Bit value: 1
        kill(ppid, SIGUSR2);
      }
    }
  }
}

void main_child()
{
  read_input();
  send_bits();
}

void signal_handler(int sig)
{
  static int i = 0;
  static int j = 7;
  static char received_text[TSIZE + 1]; // +1 and not +2, because \n is already deleted;
                                        // the array is initialized with 0s
  if (sig == SIGUSR2) {
    received_text[i] = received_text[i] | (1 << j); 
  }
  
  j--;
  
  if (j < 0) {
    if (received_text[i] == 0) {
      printf("%s\n", received_text);
      exit(EXIT_SUCCESS);
    }
    i++;
    j = 7;
  }
  sem_post(sem);
}

void main_parent()
{
  // Example from: https://www.gnu.org/software/libc/manual/html_node/Sigaction-Function-Example.html
  // http://man7.org/linux/man-pages/man2/sigaction.2.html
  struct sigaction my_action;
  // Struct for my_action
  my_action.sa_handler = signal_handler;
  sigemptyset (&my_action.sa_mask); // No signals are masked except itself
  sigaddset (&my_action.sa_mask, SIGUSR1);
  sigaddset (&my_action.sa_mask, SIGUSR2);
  my_action.sa_flags = 0;

  sigaction (SIGUSR1, &my_action, NULL);
  sigaction (SIGUSR2, &my_action, NULL);
  
  sem_post(sem);
  
  while (1) {
    pause();
  }
}
  // waitpid (pid, 0, 0); // http://man7.org/linux/man-pages/man2/waitpid.2.html

int main(int argc, char **argv)
{
  // http://man7.org/linux/man-pages/man3/sem_open.3.html
  sem = sem_open("ack", O_CREAT, 0600, 0); // ack = acknowledge; semaphore is set to 0
  pid_t pid;
  pid = fork();
  switch (pid) {
    case -1: perror("fork");
      exit(EXIT_FAILURE);
    case 0: main_child();
      break;
    default: main_parent(); 
      break;
  }
}