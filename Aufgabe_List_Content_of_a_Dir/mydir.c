// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// This program imitates the ls command, which is used
// to list the content of a directory.
// The program is called in terminal as follows: 
// mydir [-r] [name]
// If directory name is omitted, the content of the 
// current dir is printed. If the flag r is chosen, 
// the output should correspond to the output of 
// the Unix command /bin/ls -1aFR  

#define _POSIX_C_SOURCE 200112L
#define _BSD_SOURCE

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>


// size_t is an unsigned integer type of at least 16 bit

void print_current_dir();
void print_current_dir_rec(const char* dirname);

int main(int argc, char **argv)
{
  char* pathname;
  int rec = 0;
  
  if (argc < 2) {
  	pathname = ".";
  } 
  else {
    if(strcmp(argv[1], "-r") == 0) {
      rec = 1;
      if (argc < 3) {
        pathname = ".";
      } else {
        pathname = argv[2];
      }
    } else {
  	  pathname = argv[1];
    }
  }
  
  if(chdir(pathname) == -1) {
    perror("Fehler beim Wechseln des Verzeichnisses");
    exit(EXIT_FAILURE);
  }
  if(rec) {
    print_current_dir_rec(pathname);
  } else {
    print_current_dir();
  }
}

void print_current_dir() {
  DIR* dirpt;
  if ((dirpt = opendir(".")) == NULL) {
    perror("Fehler beim Öffnen des Verzeichnisses");
    exit(EXIT_FAILURE);
  }
  
  struct dirent *next;
  struct stat statbuf;
  
  while ((next = readdir(dirpt)) != NULL) {
    printf("%s", next->d_name); //(*next).d_name; d_name is called "field of a structure"
    switch (next->d_type) {
      case DT_DIR:
        printf("/");
        break;
      case DT_FIFO:
        printf("|");
        break;
      case DT_LNK:
        printf("@");
        break;
      case DT_SOCK:
        printf("=");
        break;
      case DT_REG: 
        if ((stat (next->d_name, &statbuf) == 0) && (statbuf.st_mode & 0111)) { // 0111 im Oktalsystem
          printf("*");
        }
        break;      
    } 
    printf("\n");
  }
  closedir(dirpt);
}

void print_current_dir_rec(const char* dirname) {
  printf("%s:\n", dirname);
  print_current_dir();
  printf("\n");
  
  DIR* dirpt;
  if ((dirpt = opendir(".")) == NULL) {
    perror("Fehler beim Öffnen des Verzeichnisses");
    exit(EXIT_FAILURE);
  }
  
  struct dirent *next;
  
  while ((next = readdir(dirpt)) != NULL) {
    if ((next->d_type == DT_DIR) && 
        (strcmp(next->d_name, ".") != 0) && 
        (strcmp(next->d_name, "..") != 0)) {
      char newname[strlen(dirname) + 1 + strlen(next->d_name) + 1];
      strcpy(newname, dirname);
      strcat(newname, "/");
      strcat(newname, next->d_name);
      chdir(next->d_name);
      print_current_dir_rec(newname);
      chdir("..");
    } 
  }
  closedir(dirpt);
}

// Checking if a directory exists in Unix (system call):
// https://stackoverflow.com/questions/3828192/checking-if-a-directory-exists-in-unix-system-call