// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// This program counts the number of regular files
// of certain size in a directory


#define _POSIX_C_SOURCE 200112L
#define _BSD_SOURCE

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>


// size_t is an unsigned integer type of at least 16 bit

void count_current_dir();
void count_current_dir_rec();

long counters[8] = {0, 0, 0, 0, 0, 0, 0, 0};

int main(int argc, char **argv)
{
  char* pathname;
  
  if (argc < 2) {
  	printf("Verzeichnisparameter fehlt!\n");
    exit(EXIT_FAILURE);
  } 
  else {
  	pathname = argv[1];
  }
  
  if(chdir(pathname) == -1) {
    perror("Fehler beim Wechseln des Verzeichnisses");
    exit(EXIT_FAILURE);
  }
  
  count_current_dir_rec();
  
  printf("\n");
  printf("0 Bytes <= FS(f) < 512 Bytes: %ld\n", counters[0]);
  printf("512 Bytes <= FS(f) < 1 KiB: %ld\n", counters[1]);
  printf("1 KiB <= FS(f) < 2 KiB: %ld\n", counters[2]);
  printf("2 KiB <= FS(f) < 4 KiB: %ld\n", counters[3]);
  printf("4 KiB <= FS(f) < 8 KiB: %ld\n", counters[4]);
  printf("8 KiB <= FS(f) < 64 KiB: %ld\n", counters[5]);
  printf("64 KiB <= FS(f) < 1 MiB: %ld\n", counters[6]);
  printf("1 MiB <= FS(f): %ld\n", counters[7]);
}

void count_current_dir() {
  DIR* dirpt; // https://www.gnu.org/software/libc/manual/html_node/Opening-a-Directory.html
  if ((dirpt = opendir(".")) == NULL) {
    perror("Fehler beim Öffnen des Verzeichnisses");
    exit(EXIT_FAILURE);
  }
  
  struct dirent *next;
  struct stat statbuf;
  
  while ((next = readdir(dirpt)) != NULL) { // http://man7.org/linux/man-pages/man3/readdir.3.html
    if (next->d_type == DT_REG) { // DT_REG = regular file
      if (stat(next->d_name, &statbuf) == 0) { // http://man7.org/linux/man-pages/man2/stat.2.html
        if (statbuf.st_size < 512) {
          counters[0]++;
        }
        else if (statbuf.st_size < 1024) {
          counters[1]++;
        }
        else if (statbuf.st_size < 2048) {
          counters[2]++;
        }
        else if (statbuf.st_size < 4096) {
          counters[3]++;
        }
        else if (statbuf.st_size < 8192) {
          counters[4]++;
        }
        else if (statbuf.st_size < 65536) {
          counters[5]++;
        }
        else if (statbuf.st_size < 1048576) {
          counters[6]++;
        }
        else if (statbuf.st_size >= 1048576) {
          counters[7]++;
        }
      }
    } 
  }
  closedir(dirpt);
}

void count_current_dir_rec() {
  count_current_dir();
  
  DIR* dirpt;
  if ((dirpt = opendir(".")) == NULL) {
    perror("Fehler beim Öffnen des Verzeichnisses");
    exit(EXIT_FAILURE);
  }
  
  struct dirent *next;
  
  while ((next = readdir(dirpt)) != NULL) {
    if ((next->d_type == DT_DIR) && 
        (strcmp(next->d_name, ".") != 0) && // http://man7.org/linux/man-pages/man3/strcmp.3.html
        (strcmp(next->d_name, "..") != 0)) 
    {
      chdir(next->d_name);
      count_current_dir_rec();
      chdir("..");
    } 
  }
  closedir(dirpt);
}

// Checking if a directory exists in Unix (system call):
// https://stackoverflow.com/questions/3828192/checking-if-a-directory-exists-in-unix-system-call