// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// https://en.wikipedia.org/wiki/Include_guard
// Header guard is used for avoiding the situation in which
// the same header file is included more than once in a program
#ifndef MY_MUTEX_H 
#define MY_MUTEX_H

void mutex_lock (unsigned char *lock);
void mutex_unlock (unsigned char *lock);

#endif /* MY_MUTEX_H */