// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// This program tests the functions mutex_lock and mutex_unlock contained in my_mutex.c 
// The variable x incremented by the increment function serves as a critical section for testing

#include <pthread.h>
#include <stdio.h>
#include "my_mutex.h"

// Pthreads example taken from: http://timmurphy.org/2010/05/04/pthreads-in-c-a-minimal-working-example/

void increment() {
  // "A static variable inside a function keeps its value between invocations": 
  // https://stackoverflow.com/questions/572547/what-does-static-mean
  static int x = 0; 
  static unsigned char lock = 0;
  mutex_lock(&lock);
  x++;
  printf ("Counter = %d\n", x);
  mutex_unlock(&lock);
}

// This function is run by the second thread
void *thread_main(void *ptr)
{
  for (int i = 0; i < 20; i++) {
    increment();
  }
  return NULL;
}

int main()
{
  pthread_t thread; 
  
  // Man page: http://man7.org/linux/man-pages/man3/pthread_create.3.html
  if(pthread_create(&thread, NULL, thread_main, NULL)) { 
    fprintf(stderr, "Error creating thread\n");
    return 1;
  }
  
  for (int i = 0; i < 20; i++) {
    increment();
  }
  
  // Man page: http://man7.org/linux/man-pages/man3/pthread_join.3.html
  if(pthread_join(thread, NULL)) {
    fprintf(stderr, "Error joining thread\n");
    return 1;
  }
  
  return 0;
}

// 1.4  GCC Compilation Process: https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html