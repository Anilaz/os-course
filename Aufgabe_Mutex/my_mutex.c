// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// MUTEX -- mutual exclusion; used to prevent race condition
// Mutex is used to provide exclusive access to a critical section
#include "my_mutex.h" // Error if definitions in my_mutex.c and my_mutex.h are different

int my_tsl(unsigned char *adr);

void mutex_lock (unsigned char *lock) {
  while (my_tsl (lock));
}

void mutex_unlock (unsigned char *lock) {
  *lock = 0;
}