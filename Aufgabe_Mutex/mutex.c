// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

#include "mutex.h" // Returns error if definitions in mutex.c and mutex.h are different

int tsl(unsigned char *adr);

// From slides to the course Betriebssysteme: IPC-Introduction_WS15, S. 29
void mutex_lock (unsigned char *lock) { // values of lock are 0 or 1, so use char - it takes only 1 Byte
  while (tsl (lock)); // Wait as long as the value of lock is 1
}

void mutex_unlock (unsigned char *lock) {
  *lock = 0;
}