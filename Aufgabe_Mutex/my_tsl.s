# Solutions to the Operating Systems course at the University of Tuebingen
# Author: Zalina Baysarova

# This is an assembler program for the AMD64-Architektur
# It implements the function tsl() (Test and Set)

	.file	"my_tsl.c"
	.text
	.p2align 4,,15
.globl my_tsl
	.type	my_tsl, @function
my_tsl:
.LFB0:
	.cfi_startproc
	# Return value of my_tsl is integer; by using the following instruction,
	# the upper bits of %eax are set to 0, except the least significant bit --> 1
	movl $1, %eax
	# Exchange the unsigned char value from (%rdi) with the last 8 bits of %eax --> %al
	xchgb (%rdi), %al
	# Return the value of %eax: its last 8 bits comes from %al, other bits are set to 0
	ret
	.cfi_endproc
.LFE0:
	.size	my_tsl, .-my_tsl
	.ident	"GCC: (GNU) 4.4.7 20120313 (Red Hat 4.4.7-4)"
	.section	.note.GNU-stack,"",@progbits
