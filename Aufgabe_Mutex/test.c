// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova
// This file was provided by the course. 
// It should be executed with -O2 -S -o test.s.
// The resulting assembly file should be analyzed,
// and, based on it, a tsl() function should be
// written in assembler.

int test(unsigned char *adr)
  {
  if (*adr) {
    (*adr)++;
  }
return 0; }

// https://wiki.cdot.senecacollege.ca/wiki/X86_64_Register_and_Instruction_Quick_Start
// movl: http://www.hep.wisc.edu/~pinghc/x86AssmTutorial.htm
