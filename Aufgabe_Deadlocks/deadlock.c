// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// This program implements the Banker's algorithm, which is used (in theory)
// for preventing deadlocks (situations in which the resource distribution done
// unwisely leads to deadlocks -- the processes are stuck)

#include <stdio.h>
#include <string.h>


#define NR 3 // Number of resources
#define NP 5 // Number of processes
int exist[NR] = {7, 2, 6};
int available[NR];

int current[NP][NR] = { 
  {0, 1, 0}, 
  {2, 0, 0}, 
  {3, 0, 3}, 
  {2, 1, 1},
  {0, 0, 2}
};

int request[NP][NR] = { 
  {0, 0, 0}, 
  {2, 0, 2}, 
  {0, 0, 0}, 
  {1, 0, 0},
  {0, 0, 2}
 };

// Compute available resources
void compute_available()
{
  // First, copy to the available vector number of existing resources
  for (int x = 0; x < NR; x++) {
    available[x] = exist[x];
  }
  // Then, subtract from these resources the ones that are currently allocated
  for (int i = 0; i < NP; i++) {
    for (int j = 0; j < NR; j++) {
      available[j] -= current[i][j];
    }
  }
}

void display_state() 
{
  printf("Available:\n");
  for (int i = 0; i < NR; i++) {
    printf("%3d", available[i]); 
  }
  printf("\n");
  printf("\n");

  printf("Current:\n");
  for (int i = 0; i < NP; i++) {
    for (int j = 0; j < NR; j++) {
      printf("%3d", current[i][j]);
    }
    printf("\n");
  }
  printf("\n");
  
  printf("Request:\n");
  for (int i = 0; i < NP; i++) {
    for (int j = 0; j < NR; j++) {
      printf("%3d", request[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

int is_safe()
{
  compute_available(); // In case this function was not called before is_safe()
  char finished[NP]; // Keep track of finished processes; index number = number of a process
  memset(finished, 0, sizeof(finished)); // Set every element of the array to 0
  
  int finished_processes = 0; // Count finished processes
  int skipped_processes = 0; // Count processes that cannot be finished
  
  // Based on the banker's algorithm (Modern Operating Systems by Tanenbaum)
  while (finished_processes + skipped_processes != NP) {
    //printf("START NEW MAIN LOOP\n");
    skipped_processes = 0;
    for (int i = 0; i < NP; i++) {
      if (!finished[i]) {
        //printf("NOT FINISHED PROCESS: %d\n", i);
        char res_available = 1;
        for (int j = 0; j < NR; j++) {
          res_available = res_available && (request[i][j] <= available[j]);
        }
        if(res_available) {
          //printf("FINISHED PROCESS: %d\n", i);
          finished_processes++;
          finished[i] = 1;
          for (int j = 0; j < NR; j++) {
            available[j] += current[i][j];
            //current[i][j] = 0;
          }
          break;
        } 
        else {
          //printf("SKIPPED PROCESS: %d\n", i);
          skipped_processes++;
        }
      }
    }
  }
  compute_available(); // Restore available vector to the initial state
  return finished_processes == NP;
}

int main(int argc, char **argv)
{
  compute_available();
  display_state();
  if(is_safe()) {
    printf("The state is safe\n\n");
  }
  else {
    printf("The state is not safe\n\n");
  }
 
  return 0;
}



