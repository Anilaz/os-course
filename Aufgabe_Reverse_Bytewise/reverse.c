// Solutions to the Operating Systems course at the University of Tuebingen
// Author: Zalina Baysarova

// This program reverses a file bytewise

#define _POSIX_C_SOURCE 200112L

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>


#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// size_t is an unsigned integer type of at least 16 bit
int main(int argc, char **argv)
{
  if (argc < 2) {
    printf("Name der Datei fehlt\n");
    exit(EXIT_FAILURE);
  }
  
  char* filename = argv[1];
  size_t revnamelen = strlen(filename) + 4 + 1; // +1 for 0 = End of the array
  
  char revname[revnamelen];
  strcpy(revname, filename);
  strcat(revname, ".rev"); // char *strcat(char *dest, const char *src): CONST CHAR* can be written directly into the code.
  
  int fd;
  int fdrev;
  off_t filesize; // in bytes  
  if ((fd = open(filename, O_RDONLY)) == -1) {
    perror("Fehler beim Öffnen der Eingabedatei");
    exit(EXIT_FAILURE);
  }
  
  filesize = lseek(fd, 0, SEEK_END);
  // also possible using stat function
  
  if ((fdrev = creat(revname, 0666)) == -1) { 
    perror("Fehler beim Erstellen der Reverse-Datei");
    exit(EXIT_FAILURE);
  }

  char buffer[1];
  
  for (off_t i = filesize - 1; i >= 0; i--) {
    lseek(fd, i, SEEK_SET);
    read(fd, buffer, 1);
    write(fdrev, buffer, 1);                        
  }
}

